package com.techuniversity.emp;

import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedido;
import com.techuniversity.emp.utils.Utilidades;
//import org.graalvm.compiler.core.common.util.Util;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void testCadena(){
        String esperado = "L.U.Z D.E.L S.O.L";
        assertEquals(esperado,empleadosController.getCadena("luz del sol", "."));
    }

    @Test
    public void testBadSeparator(){
        try{
            Utilidades.getCadena("Cristina Ordoño","..");
            fail("Se esperaba Bad Separator");
        }catch (BadSeparator bs){}
    }

    @ParameterizedTest
    @ValueSource(ints ={1,3,5,-3,15,Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings ={"",""})
    public void testEstaBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullSource
    @EmptySource
    public void testEstaBlanconull(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    public void testEstaBlanconull1(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    // EnumSource - el origen del valor
    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    public void testValorarEstado(EstadosPedido estado){
        assertTrue(Utilidades.valorarEstadoPedido(estado));
    }
}
