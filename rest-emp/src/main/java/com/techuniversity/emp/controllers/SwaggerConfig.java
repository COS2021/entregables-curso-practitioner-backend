package com.techuniversity.emp.controllers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

//Docket es el que sabe lo que quieres hacer publico, .apis para decir que porcion de codigo quiero utilizar
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket apiDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.techuniversity.emp"+ ""))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(getApiInfo());
    }
// Pasamos varios parametros al constructor
    private ApiInfo getApiInfo(){
       return new ApiInfo(
               "The Rest Emp project", "Devuelve informacion de empleados", "1.0", "https://codmind.com/temrs",
            new Contact("Demo", "https://apis@demo.com", "cris@mail.com"), "license", "LICENSE URL", Collections.emptyList()
        );
    }
}
