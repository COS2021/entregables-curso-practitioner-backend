package com.techuniversity.prod.productos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

// Spring con esta anotacion entiende que esta interfaz es el repositorio
@Repository
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {

}
