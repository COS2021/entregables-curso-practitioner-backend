package com.techuniversity.prod;

import com.techuniversity.prod.productos.ProductoModel;
import com.techuniversity.prod.productos.ProductoRepository;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ProductoRepoIntegrationTest {
    @Autowired
    ProductoRepository productoRepository;

    @Test
    public void testFindAll(){
        List<ProductoModel> productos = productoRepository.findAll();
        assertTrue(productos.size() > 0);
    }

    @LocalServerPort
    private int port;
    // Plantilla testeo de servicios rest
    TestRestTemplate testRestTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();

    @Test
    public void testPrimerProducto() throws Exception{
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);
        // Simula una invocación al servicio REST
        ResponseEntity<String> response = testRestTemplate.exchange(
                crearUrlConPuerto("/productos/productos/60acaa0a1f58ca36850f0df2"),
                        HttpMethod.GET,entity,String.class);
        String expected = "{\"id\":\"60acaa0a1f58ca36850f0df2\"," + "\"nombre\":\"Producto 2\",\"descripcion\":\"Descripción Producto 2\"," + "\"precio\": 33.5}";
        JSONAssert.assertEquals(expected, response.getBody(), false);
    }

    private String crearUrlConPuerto(String url){
        return "http://localhost:" + port + url;

    }

}
