package com.techuniversity.prod.servicios;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ServiciosService {
    MongoCollection<Document> servicios;
    // Puntero hacia una colección de mongodb
    private static MongoCollection<Document> getServiciosCollection(){
        // Le pasamos una ruta de conexión a la bbdd con su puerto 27017
        ConnectionString cs = new ConnectionString("mongodb://localhost:27017");
        // Conexion de cliente (apply, le aplica la ruta que tenemos y retry si no le funciona que lo vuelva a intentar)
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        // Creamos al cliente
        MongoClient cliente = MongoClients.create(settings);
        MongoDatabase database = cliente.getDatabase("backMayo");
        return database.getCollection("servicios");
    }
    // Devuelve todo
    public static List getAll(){
        // Dame el puntero hacia la colección servicios
        MongoCollection<Document> servicios = getServiciosCollection();
        List list = new ArrayList();
        // Llamada a la funcion find que devuelve la colecion document de tipo finditerable, si es un find sin nada es como un select *
        FindIterable<Document> iterdoc = servicios.find();
        Iterator iterador = iterdoc.iterator();
        // Mientras iterador tenga otro vamos a añadir a lista cada uno de los elementos encontrados en la coleccion y devolvemos la colección
        while (iterador.hasNext()){
            list.add(iterador.next());
        }
        return list;
    }
    // Insert al que le entrará el json a insertar (cualquier estructura de json)
    public static void insert(String cadenaServicio) throws Exception{
        // Creamos el documento (lo parsea para crear el documento)
        Document doc = Document.parse(cadenaServicio);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertOne(doc);
    }
    // Insert de varios registros (servicios) a la vez
    public static void insertBatch(String cadenaServicios) throws Exception{
        // Creamos los documentos (lo pasea para crear los documentos)
        Document doc = Document.parse(cadenaServicios);
        List<Document> lstServicios = doc.getList("servicios",Document.class);
        MongoCollection<Document> servicios = getServiciosCollection();
        servicios.insertMany(lstServicios);
    }
    // Filtros, devuelve una lista document
    public static List<Document> getFiltrados(String cadenaFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        // Array donde ubico los documentos que encuentre
        List lista = new ArrayList();
        Document docFiltro = Document.parse(cadenaFiltro);
        FindIterable<Document> iterdoc = servicios.find(docFiltro);
        Iterator iterador = iterdoc.iterator();
        // Mientras iterador tenga otro vamos a añadir a lista cada uno de los elementos encontrados en la coleccion y devolvemos la colección
        while (iterador.hasNext()){
            lista.add(iterador.next());
        }
        return lista;
    }
    public static List<Document> getFiltradosPeriodo(Document docFiltro){
        MongoCollection<Document> servicios = getServiciosCollection();
        // Array donde ubico los documentos que encuentre
        List lista = new ArrayList();
        FindIterable<Document> iterdoc = servicios.find(docFiltro);
        Iterator iterador = iterdoc.iterator();
        // Mientras iterador tenga otro vamos a añadir a lista cada uno de los elementos encontrados en la coleccion y devolvemos la colección
        while (iterador.hasNext()){
            lista.add(iterador.next());
        }
        return lista;
    }
    public static void update(String filtro, String valores){
        MongoCollection<Document> servicios = getServiciosCollection();
        Document docFiltro = Document.parse(filtro);
        Document docValores = Document.parse(valores);
        // updateone si solo quiero modificar un registro si no sería updatemany
        servicios.updateOne(docFiltro,docValores);
    }
}
