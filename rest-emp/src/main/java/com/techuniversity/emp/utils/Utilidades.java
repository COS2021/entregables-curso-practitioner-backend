package com.techuniversity.emp.utils;

public class Utilidades {
    // Ante un texto que me introduzcan, la función va a formatearlo
    public static String getCadena(String texto, String separador) throws BadSeparator{
        if ((separador.length() > 1) || separador.trim().equals(" ")) throw new BadSeparator();
        // Declaro array de caracteres y lo convierto a mayúsculas y lo devuelve en array
        // de caracteres
        char[] letras = texto.toUpperCase().toCharArray();
        // Itera por todas las letras del texto que me pasen
        // += Le añado (concateno)
        String resultado = "";
        for(char letra: letras){
            if (letra != ' '){
                resultado += letra + separador;
            }else{
                // Trim ante una cadena que tenga espacios delante o detrás, te devuelve
                // la cadena sin espacios
                if (!resultado.trim().equals("")){
                    // Cogemos solo la palabra sin el espacio que las separe
                    resultado = resultado.substring(0,resultado.length() - 1);
                }
                resultado += letra;
            }
        }
        // Si termina con el separador.
        if (resultado.endsWith(separador)){
            resultado = resultado.substring(0,resultado.length() - 1);
        }
        return resultado;
    }
    // %2 - Dividido entre 2
    public static boolean esImpar(int numero){
        System.out.println(numero % 2);
        return (numero % 2 != 0 );
    }

    public static boolean estaBlanco(String texto){
        return ((texto == null)) || texto.trim().isEmpty();

    }

    // Ordinal, por defecto a la lista del enum le da valores de 0 a los que haya...
    public static boolean valorarEstadoPedido(EstadosPedido estado){
        int valor = estado.ordinal();
        return ((valor >= 0) && (valor <= 5));
    }
}
