package com.techuniversity.prod.productos;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

//import java.awt.print.Pageable;
import java.util.List;
import java.util.Optional;

// Esta clase va a actuar como servicio
@Service
public class ProductoService {
    @Autowired
    ProductoRepository productoRepository;

    // Funcion que devuelve los productos de la coleccion
    public List<ProductoModel> findAll(){
        return productoRepository.findAll();
    }

    // Encontrar un documento (optional, devuelve el objeto si lo tiene y te permite preguntar si lleva informacion o no)
    public Optional<ProductoModel> findByid(String id){
        return productoRepository.findById(id);
    }

    // Insertara el producto de entrada y devolvere el producto insertado con su id, save es hacer un insert o un update
    // dependiendo de si el producto existe o no en bbdd
    public ProductoModel save(ProductoModel producto){
        return productoRepository.save(producto);
    }

    // Devuelve true o false si encuentra o no el documento que borra
    public boolean deleteProducto(ProductoModel producto){
        try {
            productoRepository.delete(producto);
            return true;
        } catch (Exception ex){
            return false;
        }
    }
    // Devuelve una lista de productos paginable
    public List<ProductoModel> findPaginado(int page){
        // Pageable gestiona la paginación pasando la pagina que quiero y cuantas paginas
        Pageable pageable = PageRequest.of(page,3);
        Page<ProductoModel> pages = productoRepository.findAll(pageable);
        List<ProductoModel> productos = pages.getContent();
        return productos;
    }
}
